# .bashrc

# Source global definitions
if [ -f /etc/bash.bashrc ]; then
. /etc/bash.bashrc
fi

if [ -f /etc/bashrc ]; then
. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
#export PATH=$PATH:/usr/local/cuda-7.0/bin
#export LD_LIBRARY_PATH=:/usr/local/cuda-7.0/lib64
#export PATH=/usr/local/cuda/bin:${PATH}
#PATH=${CUDA_HOME}/bin:${PATH}
#export PATH
#export PATH=$PATH:/usr/local/cuda/bin
#export LD_LIBRARY_PATH=:/usr/local/cuda/lib64

#Android sdk
#PATH=$PATH:$HOME/bin/AndroidSDK:$HOME/bin/android-sdk-linux/tools:$HOME/bin/android-sdk-linux/platform-tools
#export PATH

#colorschem base-16
# Base16 Shell
BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
