-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local map = vim.keymap.set

map("t", "<Esc>", "<C-\\><C-n>")
map("n", "ü", "<C-]>")
map("n", "Ü", "<C-O>")
