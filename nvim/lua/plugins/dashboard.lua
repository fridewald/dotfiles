return {
  "nvimdev/dashboard-nvim",
  opts = function(_, opts)
    -- create the dashboard header
    -- http://www.patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=Guten%20Tag
    local logo = [[
    ██████╗ ██╗   ██╗████████╗███████╗███╗   ██╗    ████████╗ █████╗  ██████╗ 
   ██╔════╝ ██║   ██║╚══██╔══╝██╔════╝████╗  ██║    ╚══██╔══╝██╔══██╗██╔════╝ 
   ██║  ███╗██║   ██║   ██║   █████╗  ██╔██╗ ██║       ██║   ███████║██║  ███╗
   ██║   ██║██║   ██║   ██║   ██╔══╝  ██║╚██╗██║       ██║   ██╔══██║██║   ██║
   ╚██████╔╝╚██████╔╝   ██║   ███████╗██║ ╚████║       ██║   ██║  ██║╚██████╔╝
    ╚═════╝  ╚═════╝    ╚═╝   ╚══════╝╚═╝  ╚═══╝       ╚═╝   ╚═╝  ╚═╝ ╚═════╝
  ]]

    logo = string.rep("\n", 8) .. logo .. "\n\n"
    opts.config.header = vim.split(logo, "\n")
  end,
}
