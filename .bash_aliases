#-------------------
# Personnal Aliases
#-------------------

type nvim >/dev/null 2>&1;
if [[ $? == 0 ]]; then
    alias vim='nvim';
else
    type nvim.appimage >/dev/null 2>&1;
    if [[ $? == 0 ]]; then
        alias vim='nvim.appimage'
    fi;
fi;
#alias rm='rm -i'
#alias cp='cp -i'
#alias mv='mv -i'
# -> Prevents accidentally clobbering files.
alias mkdir='mkdir -p'

alias h='history'
alias j='jobs -l'
alias which='type -a'

# Pretty-print of some PATH variables:
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'


alias du='du -kh'    # Makes a more readable output.
alias df='df -kTh'

# make xclip usefull for wayland
alias xclip='xclip -selection c'

#-------------------------------------------------------------
# The 'ls' family (this assumes you use a recent GNU ls).
#-------------------------------------------------------------
# Add colors for filetype and  human-readable sizes by default on 'ls':
alias ls='ls -h --color'
alias lx='ls -lXB'         #  Sort by extension.
alias lk='ls -lSr'         #  Sort by size, biggest last.
alias lt='ls -ltr'         #  Sort by date, most recent last.
alias lc='ls -ltcr'        #  Sort by/show change time,most recent last.
alias lu='ls -ltur'        #  Sort by/show access time,most recent last.

# The ubiquitous 'll': directories first, with alphanumeric sorting:
alias ll='ls -lv' # --group-directories-first"
alias lm='ll |more'        #  Pipe through 'more'
alias lr='ll -R'           #  Recursive ls.
alias la='ll -A'           #  Show hidden files.
alias tree='tree -Csuh'    #  Nice alternative to 'recursive ls' ...

#-------------------------------------------------------------
# Tailoring 'less'
#-------------------------------------------------------------

alias less='less -N'
alias more='less'
export PAGER='/usr/bin/less'
export LESSCHARSET='latin1'
export LESSOPEN='|/usr/bin/lesspipe.sh %s 2>&-'
                # Use this if lesspipe.sh exists.
export LESS='-i -w  -z-4 -g -M -X -F -R -P%t?f%f \
    :stdin .?pb%pb\%:?lbLine %lb:?bbByte %bb:-...'
# -e -N

# LESS man page colors (makes Man pages more readable).
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

#-------------------------------------------------------------
# own stuff
#-------------------------------------------------------------
alias dnf-up='sudo dnf update -y'
alias apt-up='sudo apt update && sudo apt upgrade -y'
#alias python='/usr/bin/python3'

# Python alias
alias python=python3

# shortcut for cd
alias ..="cd .."

# open -> xdg-open, override open program
alias oopen=/usr/bin/open
alias open=/usr/bin/xdg-open

# git log alias
alias gitlog="/usr/bin/git log --graph --decorate --all --oneline"
alias glolr="/usr/bin/git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' HEAD @{upstream}"

# jupyter
alias jp="jupyter-notebook --browser=firefox --port=8000 --ip=127.0.0.1"
alias jpl="jupyter lab --browser=firefox --port=8000 --ip=127.0.0.1"
alias jp_remote="jupyter-notebook --no-browser --port=8888 --ip=127.0.0.1"
#_completion_loader ssh
#complete -F _ssh sshjup

alias zbar='zbarcam'

# ssh login for jupyter
alias sshjup="ssh -L 8888:localhost:8888"

# vimconfig
alias vimconfig="vim ~/.vimrc"

# for printing
alias lpd="lp  -o sides=two-sided-long-edge"

# pandoc for pdf
panpdf() {
    pandoc -o ${1%.*}.pdf  --pdf-engine=xelatex $1
}

# docker compose
alias docker-compose="docker compose"
# terraform
alias tf="terraform"
# exercism
alias exm="exercism"
