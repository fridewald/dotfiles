## install

```
git clone https://gitlab.com/fridewald/dotfiles.git ~/.dotfiles
```

### oh-my-zsh
install `oh-my-zsh` [here](https://github.com/ohmyzsh/ohmyzsh)

### base16 shell
install `base16 shell` [here](https://github.com/chriskempson/base16-shell)

### linking of dot files
```
ln -s .dotfiles/.zshrc ./
ln -s .dotfiles/.bash_aliases ./
ln -s .dotfiles/.bash_exports ./
ln -s .dotfiles/.tmux.conf ./
ln -s .dotfiles/.gitconfig ./
```
